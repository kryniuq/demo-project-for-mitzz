package com.example.demo.controller;

import com.example.demo.model.Person;
import com.example.demo.repository.PersonRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.Optional;

@RestController
@RequestMapping("/api/persons")
@CrossOrigin
public class PersonController {

    @Autowired
    private PersonRepository personRepository;

    @GetMapping
    public ResponseEntity<List<Person>> getAllPersons() {
        List<Person> allPersons = personRepository.findAll();
        return new ResponseEntity<>(allPersons, HttpStatus.OK);
    }

    @GetMapping("/{id}")
    public ResponseEntity<Person> getPersonById(@PathVariable long id) {
        Optional<Person> person = personRepository.findById(id);
        return new ResponseEntity<>(person.get(), HttpStatus.OK);
    }

    @GetMapping(params = {"firstName", "age"})
    public ResponseEntity<List<Person>> getPersonsByFirstNameAndAge(@RequestParam String firstName, @RequestParam Integer age) {
        List<Person> persons = personRepository.findByFirstNameAndAge(firstName, age);
        return new ResponseEntity<>(persons, HttpStatus.OK);
    }
}
