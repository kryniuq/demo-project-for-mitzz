package com.example.demo;

import com.example.demo.model.Person;
import com.example.demo.repository.PersonRepository;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.Bean;

@SpringBootApplication
public class DemoApplication {

    public static void main(String[] args) {
        SpringApplication.run(DemoApplication.class, args);
    }

    @Bean
    CommandLineRunner initDatabase(PersonRepository repository) {
        return args -> {
            repository.save(new Person("John", "Wick", 32));
            repository.save(new Person("Donald", "Trump", 74));
            repository.save(new Person("Dominik", "Tworek", 23));
            repository.save(new Person("Andrzej", "Lenarcik", 32));
            repository.save(new Person("Andrzej", "Duda", 32));
        };
    }
}
